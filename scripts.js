//Query specific vehicle
$.post(rootloc + 'queryVehicle', {'vehicle_id': ''});


/**
	Fix Special Page not proportionate display.
	https://imagesloaded.desandro.com/
	
	Note: this is also applicable in VLPS
	
	You need to add the imagesloaded script before using the script below
**/

/**
* @DOM SELECTOR		.specials-widget
* 
* 
**/
  
$('.specials-widget').lazyload({
	load: function() {
		var maxHeight = -1;
		$('.specials-widget').imagesLoaded(function() {
			$('.specials-widget').each(function() {
				maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
			});

			$('.specials-widget').each(function() {
				$(this).height(maxHeight);
			});
		});			
	}
});



